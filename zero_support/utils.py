"""
Copyright 2020 Craig Sherstan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import hashlib
import json
import os
import shutil
import subprocess
import numpy.random

import git
from git import Repo
import json


def file_exists(filename):
    return os.path.isfile(filename)


def dir_exists(dir):
    return os.path.isdir(dir)


def strip_extension(path):
    idx = path.rfind('.')
    return path[0:idx]


def get_data_dir():
    return os.environ["DATA_DIR"]


def get_file_dir(filename):
    """
    Ex. /tmp/blargh/test.txt -> /tmp/blargh
    :param filename:
    :return:
    """
    return os.path.split(filename)[0]


def get_filename(path):
    """
    Ex. /tmp/blargh/test.txt -> test.txt
    :param path:
    :return:
    """
    return os.path.basename(path)


def get_split_filename(path):
    """
    Ex. /tmp/blargh/test.txt -> ('test', 'txt')
    :param path:
    :return:
    """
    fname = get_filename(path)
    return os.path.splitext(fname)


def get_ext(path):
    """
    Ex. /tmp/blargh/test.txt -> 'txt'
    :param path:
    :return:
    """
    return get_split_filename(path)[1]


def get_all_dir(path):
    return [os.path.join(path, item)
            for item in os.listdir(path) if os.path.isdir(os.path.join(path, item))]


def get_git_revision_hash(parent_dir, safe=True):
    sha = None
    try:
        repo = Repo(path=parent_dir, search_parent_directories=True)
        sha = repo.commit().hexsha
    except git.exc.InvalidGitRepositoryError:
        print("Warning: Invalid git repo")

        # if safe:
        # assert not repo.is_dirty()

    return sha


def get_git_revision_short_hash(safe=True):
    repo = Repo(search_parent_directories=True)

    # if safe:
    # assert not repo.is_dirty()

    # gitpython doesn't seem to provide a short hash
    return subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).decode('UTF-8').rstrip()


def assert_git_clean():
    repo = Repo(search_parent_directories=True)
    assert not repo.is_dirty()


def argparse_to_dict(parser):
    ret = {}
    for a in parser._actions:
        ret[a.dest] = a.default

    return ret


def get_short_hash_from_dict(item):
    return hashlib.md5(json.dumps(item, sort_keys=True).encode('UTF-8')).hexdigest()[0:8]


def rand_in_range(low, high, count=None):
    return numpy.random.random(count) * (high - low) + low


class open_create:
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

        fdir = get_file_dir(self.args[0])
        if not os.path.isdir(fdir):
            os.makedirs(fdir)

    def __enter__(self):
        self.file = open(*self.args, **self.kwargs)
        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()


def safe_dir_create(dir, delete=False):
    if delete and os.path.isdir(dir):
        shutil.rmtree(dir)

    if not os.path.isdir(dir):
        try:
            os.makedirs(dir)
        except FileExistsError:
            pass


def json_dump(obj, fname):
    with open_create(fname, "w") as fp:
        json.dump(obj, fp, sort_keys=True, indent=4, separators=(',', ': '))


def json_load(fname):
    with open(fname, "r") as fp:
        data = json.load(fp)

    return data


def filter_dict(keys, the_dict):
    """
    Returns a new dictionary with only the keys specified.
    :param keys: The keys to copy into the new dictionary
    :param the_dict: The source dictionary
    :return:
    """
    return {key: item for key, item in the_dict.items() if key in keys}


def filter_out_dict(keys_to_remove, the_dict):
    """
    Returns a new dictionary with the specified keys removed.
    :param keys_to_remove: Keys to exclude from the new dict
    :param the_dict: The source dictionary
    :return:
    """
    return {key: item for key, item in the_dict.items() if key not in keys_to_remove}


def gen_init(odir):
    ofile = open(os.path.join(odir, "__init__.py"), "w")
    ofile.close()
