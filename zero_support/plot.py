"""
Copyright 2020 Craig Sherstan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import numpy as np
import matplotlib


def mse(target, estimate):
    assert target.shape == estimate.shape
    return ((np.array(target) - np.array(estimate)) ** 2) / len(target)


def rmse(target, estimate):
    return np.sqrt(mse(target, estimate))


def avg_by_bin(data, interval):
    original_len = len(data)
    data = data[0:int(len(data) / interval) * interval]
    if len(data) < original_len:
        print("warning: avg_by_bin truncated data")
    y = np.mean(np.array(data).reshape(-1, interval), axis=1)
    x = np.linspace(interval, len(y) * interval, num=len(y))
    return x, y


def color_y_axis(ax, color):
    for t in ax.get_yticklabels():
        t.set_color(color)


# -----------------------------------------------------------
# http://stackoverflow.com/questions/16992038/inline-labels-in-matplotlib


# Label line with line2D label data
def labelLine(line, x, label=None, align=True, **kwargs):
    ax = line.get_axes()
    xdata = line.get_xdata()
    ydata = line.get_ydata()

    if (x < xdata[0]) or (x > xdata[-1]):
        print('x label location is outside data range!')
        return

    # Find corresponding y co-ordinate and angle of the
    ip = 1
    for i in range(len(xdata)):
        if x < xdata[i]:
            ip = i
            break

    y = ydata[ip - 1] + (ydata[ip] - ydata[ip - 1]) * (x - xdata[ip - 1]) / (xdata[ip] - xdata[ip - 1])

    if not label:
        label = line.get_label()

    if align:
        # Compute the slope
        dx = xdata[ip] - xdata[ip - 1]
        dy = ydata[ip] - ydata[ip - 1]
        ang = degrees(atan2(dy, dx))

        # Transform to screen co-ordinates
        pt = np.array([x, y]).reshape((1, 2))
        trans_angle = ax.transData.transform_angles(np.array((ang,)), pt)[0]

    else:
        trans_angle = 0

    # Set a bunch of keyword arguments
    if 'color' not in kwargs:
        kwargs['color'] = line.get_color()

    if ('horizontalalignment' not in kwargs) and ('ha' not in kwargs):
        kwargs['ha'] = 'center'

    if ('verticalalignment' not in kwargs) and ('va' not in kwargs):
        kwargs['va'] = 'center'

    if 'backgroundcolor' not in kwargs:
        kwargs['backgroundcolor'] = ax.get_axis_bgcolor()

    if 'clip_on' not in kwargs:
        kwargs['clip_on'] = True

    if 'zorder' not in kwargs:
        kwargs['zorder'] = 2.5

    ax.text(x, y, label, rotation=trans_angle, **kwargs)


def labelLines(lines, align=True, xvals=None, **kwargs):
    ax = lines[0].get_axes()
    labLines = []
    labels = []

    # Take only the lines which have labels other than the default ones
    for line in lines:
        label = line.get_label()
        if "_line" not in label:
            labLines.append(line)
            labels.append(label)

    if xvals is None:
        xmin, xmax = ax.get_xlim()
        xvals = np.linspace(xmin, xmax, len(labLines) + 2)[1:-1]

    for line, x, label in zip(labLines, xvals, labels):
        labelLine(line, x, label, align, **kwargs)


def plot_avg_and_var(x, avg, var, color, ax, label, plotfunc=None, **kwargs):
    stdev = np.sqrt(var)
    shade_max = avg + stdev
    shade_min = avg - stdev

    plotfunc = plotfunc if plotfunc is not None else ax.plot
    p = plotfunc(x, avg, color=color, label=label, **kwargs)

    ax.fill_between(x, shade_min, shade_max, alpha=0.3, color=p[0].get_color())


def plot_avg_and_max_min(x, avg, max, min, color, ax, label, plotfunc=None, **kwargs):
    plotfunc = plotfunc if plotfunc is not None else ax.plot
    if color is not None:
        kwargs["color"] = color
    if label is not None:
        kwargs["label"] = label
    p = plotfunc(x, avg, **kwargs)

    if max is not None and min is not None:
        ax.fill_between(x, min, max, alpha=0.2, color=p[0].get_color(), zorder=-20)


def plot_avg_and_ci(x, avg, var, n, z, color, ax, label, plotfunc=None, **kwargs):
    stdev = np.sqrt(var)
    delta = z * stdev / np.sqrt(n - 1)
    shade_max = avg + delta
    shade_min = avg - delta

    plot_avg_and_max_min(x, avg, shade_max, shade_min, color, ax, label, plotfunc, **kwargs)


def align_yaxis(ax1, v1, ax2, v2):
    """
    https://stackoverflow.com/questions/10481990/matplotlib-axis-with-two-scales-shared-origin
    adjust ax2 ylimit so that v2 in ax2 is aligned to v1 in ax1
    """
    _, y1 = ax1.transData.transform((0, v1))
    _, y2 = ax2.transData.transform((0, v2))
    inv = ax2.transData.inverted()
    _, dy = inv.transform((0, 0)) - inv.transform((0, y1 - y2))
    miny, maxy = ax2.get_ylim()
    ax2.set_ylim(miny + dy, maxy + dy)


def set_tick_fontsize(axis, fontsize):
    for tick in axis.get_major_ticks():
        tick.label.set_fontsize(fontsize)


def set_ytick_fontsize(ax, fontsize):
    set_tick_fontsize(ax.yaxis, fontsize)


def set_xtick_fontsize(ax, fontsize):
    set_tick_fontsize(ax.xaxis, fontsize)


def set_ax_tick_fontsize(ax, fontsize):
    set_ytick_fontsize(ax, fontsize)
    set_xtick_fontsize(ax, fontsize)


def set_k_formatter(axis):
    set_divisor_formatter(axis, 1000)


def set_xaxis_k_formatter(ax):
    set_k_formatter(ax.get_xaxis())


def set_yaxis_k_formatter(ax):
    set_k_formatter(ax.get_yaxis())


def set_divisor_formatter(axis, divisor):
    axis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p: int(x / divisor)))


def set_xaxis_divisor_formatter(ax, divisor):
    set_divisor_formatter(ax.get_xaxis(), divisor)


def set_yaxis_divisor_formatter(ax, divisor):
    set_divisor_formatter(ax.get_yaxis(), divisor)


def get_divisor_formatter(divisor, type=int):
    return matplotlib.ticker.FuncFormatter(lambda x, p: type(x / divisor))


def set_formatter(axis, formatter):
    axis.set_major_formatter(formatter)


def set_xaxis_formatter(ax, formatter):
    set_formatter(ax.get_xaxis(), formatter)


def set_yaxis_formatter(ax, formatter):
    set_formatter(ax.get_yaxis(), formatter)
