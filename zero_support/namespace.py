"""
Copyright 2020 Craig Sherstan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import importlib
import inspect


class Namespace:
    # TODO: in Python3.6 (maybe earlier) this can be replaced with Enum
    class PERMISSION:
        """"""

    class WARN(PERMISSION):
        """
        Allow override, but warn
        """

    class BLOCK(PERMISSION):
        """
        Prevent any override, throw exception
        """

    class PERMISSIVE(PERMISSION):
        """
        override as you like
        """

    class SET_ONCE(PERMISSION):
        """
        The attribute cannot be overridden, but no exception will be thrown. Just a warning.
        """

    class LAZY:
        def __init__(self, fcn):
            self.fcn = fcn
            self.val = None

        def __call__(self):
            if self.val == None:
                self.val = self.fcn()
            return self.val

    def __init__(self, name="main", pkgs=None):
        self.__dict__["permission_map"] = {}
        self.name = name

        if pkgs:
            if type(pkgs) == str:
                pkgs = [pkgs]
            for pkg in pkgs:
                self.load(pkg)

    def load(self, pkgs, ns=None):
        if ns is None:
            ns = self

        if type(pkgs) == str:
            pkgs = [pkgs]

        for pkg in pkgs:
            importlib.import_module(pkg).load(ns)

    def __setitem__(self, key, value):
        self.__setattr__(key, value)

    def __setattr__(self, key, value):

        permission = Namespace.PERMISSIVE

        if key == "load":
            raise Exception("load cannot be overridden")

        if key in self.permission_map:
            existing_permission = self.permission_map[key]

            if existing_permission == Namespace.BLOCK:
                raise Exception("ERROR: Namespace {}: Cannot override key {}".format(self.name, key))
            elif existing_permission == Namespace.WARN:
                print("WARNING: Namespace {}: Overriding key {}".format(self.name, key))
            elif existing_permission == Namespace.SET_ONCE:
                print("WARNING: Namespace {}: {} is already set, will not overridden.".format(self.name, key))
                return

        # check if a permission is set. Need to make sure it's not a regular tuple.
        if type(value) is tuple and len(value) == 2 and inspect.isclass(value[1]) and issubclass(value[1],
                                                                                                 Namespace.PERMISSION):
            permission = value[1]
            value = value[0]

        self.__dict__[key] = value
        self.permission_map[key] = permission

    def __getitem__(self, item):
        ret = self.__getattribute__(item)

        if isinstance(ret, Namespace.LAZY):
            return ret()

        return ret

    def __contains__(self, item):
        return self.__dict__.__contains__(item)

    def get_or_default(self, key, default):
        if key in self:
            return self[key]
        return default
